import os
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw 

dir = "/home/user/Pictures"

def makeImg(path,number):
    try:  
        img  = Image.open(path) 
        width, height = img.size   
        img = img.resize((int(width/10), int(height/10)))
        draw = ImageDraw.Draw(img)
        font = ImageFont.truetype("Arial.ttf", 30)
        draw.text((10, 10),number,(255,255,255),font=font)
        img.save('./img/resized-'+ number +'.jpg')
        print(path," - " + number) 
    except IOError:
        pass

files = os.listdir(dir)
for pic in files:
    word = pic.find("-")
    word = word + 1
    word2 = pic.find("jpg",word)
    word2 = word2 - 1
    makeImg(dir + "/"+pic, pic[word:word2])
